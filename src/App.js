import logo from './logo.svg';
import './App.css';
import { Link } from 'react-router-dom';

function App() {
  return (
    <div className='container-fluid'>
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
          <li className="nav-item active">
            <Link to={"/"}><a className="nav-link active" href="#">formations</a></Link>
          </li>
          <li className="nav-item">
          <Link to={"/employes"}><a className="nav-link" href="#">employes</a></Link>
          </li>
          <li className="nav-item">
          <Link to={"/participations"}><a className="nav-link" href="#">participations</a></Link>
          </li>
        </ul>
      </div>
    </nav>
    </div>
  );
}

export default App;
