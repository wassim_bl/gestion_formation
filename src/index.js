import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter,Routes ,Route } from 'react-router-dom';
import FormationsList from './components/formation/FormationsList';
import EmployesList from './components/employes/EmployesList';
import AddFormation from './components/formation/AddFormation';
import AddEmploye from './components/employes/AddEmploye';
import UpdateEmploye from './components/employes/UpdateEmploye';
import UpdateFormation from './components/formation/UpdateFormation';
import ParticipationList from './components/participation/ParticipationList';
import AddParticipation from './components/participation/AddParticipation';
import UpdateParticipation from './components/participation/UpdateParticipation';
import { Provider } from 'react-redux';
import { store } from './components/Redux/Store';
import Layout from './components/Layout';



const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Provider store={store}>
  <BrowserRouter>
    <Routes>
    <Route path="/" element={<Layout/>}>
      <Route index element={<FormationsList />} />
      <Route path='/employes' element={<EmployesList />} />
      <Route path='/addEmploye' element={<AddEmploye />} />
      <Route path='/updateEmploye/:id' element={<UpdateEmploye />} />
      <Route path='/formation/add' element={<AddFormation />} />
      <Route path='/formation/update/:id' element={<UpdateFormation />} />
      <Route path='/participations' element={<ParticipationList />} />
      <Route path='/participation/add' element={<AddParticipation />} />
      <Route path='/updateparticipation/:id' element={<UpdateParticipation />} />
      </Route>
    </Routes>
  </BrowserRouter>
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
