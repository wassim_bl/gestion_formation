import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

export default function EmployesList() {
  const [employes, setEmploye] = useState([]);
  const [employesList, setEmployesList] = useState([]);
  const [nameFilter, setNameFilter] = useState('');
  const [prenomFilter, setPrenomFilter] = useState('');
  const [phoneNumberFilter, setPhoneNumberFilter] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get("http://localhost:3003/employees");
        setEmploye(response.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);

  const afficher_formartion = async (id) => {
    try {
      const partic = await axios.get(`http://localhost:3003/participation`);
      const employee = partic.data.find((emp) => emp.employee_id === id);

      if (employee && employee.formations) {
        const formations = await Promise.all(employee.formations.map(async (formation) => {
          const additionalData = await someAsyncOperation(formation);
          return (
            <li key={formation.name}>{formation.name} - {additionalData}</li>
          );
        }));

        return formations;
      } else {
        return <li>No formations available</li>;
      }
    } catch (error) {
      console.error('Error fetching data:', error);
      return <li>Error fetching formations</li>; // Handle the error appropriately
    }
  };
  const delete_employe = async (id) => {
    try {
      await axios.delete(`http://localhost:3003/employees/${id}`);
      setEmploye(employes.filter(employe => employe.id !== id));
    } catch (error) {
      console.error('Error deleting employee:', error);
    }
  }
  useEffect(() => {
    const renderData = async () => {
      const filteredEmployes = employes.filter(employee =>
        employee.name.toLowerCase().includes(nameFilter.toLowerCase()) &&
        employee.phone_number.includes(phoneNumberFilter) &&
        employee.lastname.toLowerCase().includes(prenomFilter.toLowerCase())
      );

      const newEmployesList = await Promise.all(filteredEmployes.map(async (employe, key) => (
        <tr key={key}>
          <td>{employe.name}</td>
          <td>{employe.lastname}</td>
          <td>{employe.age}</td>
          <td>{employe.phone_number}</td>
          <td>{employe.address}</td>
          <td>
            <ul>
              {await afficher_formartion(employe.id)}
            </ul>
          </td>
          <td>
            <Link to={`/updateEmploye/${employe.id}`}>
              <a href="" className='btn btn-primary'>update</a>
            </Link>
          </td>
          <td>
            <a href="" className='btn btn-danger' onClick={(e) => { e.preventDefault(); delete_employe(employe.id); }} >delete</a>
          </td>
        </tr>
      )));

      setEmployesList(newEmployesList);
    };

    renderData();
  }, [employes, nameFilter, phoneNumberFilter,prenomFilter]);

  const handleReset = ()=>{
    setNameFilter("")
    setPhoneNumberFilter("")
    setPrenomFilter("")
  }
  return (
    <div className='container'>
      <br />
      <div className='mx-auto'>
        <Link to="/addEmploye" className='btn btn-primary'>ajouter employe</Link>
      </div>
      <br /><br />
      <div className="mb-3 d-flex flex-wrap align-items-center mt-3">
        <div className="me-3">
          <label htmlFor="Nom">Nom :</label>
          <input
            type="text"
            className="form-control"
            placeholder="Nom"
            value={nameFilter}
            onChange={(e) => setNameFilter(e.target.value)}
          />
        </div>
        <div className="me-3">
          <label htmlFor="Prénom">Prénom :</label>
          <input
            type="text"
            className="form-control"
            placeholder="Prénom"
            value={prenomFilter}
            onChange={(e) => setPrenomFilter(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="telephone">télephone :</label>
          <input
            type="text"
            className="form-control"
            placeholder="telephone"
            value={phoneNumberFilter}
            onChange={(e) => setPhoneNumberFilter(e.target.value)}
          />
        </div>
        <div className="me-3">
              <div className="invisible">Actions</div>
              <button className="btn btn-info ms-3" onClick={handleReset}>Reset</button>
            </div>
      </div>
      <br /><br />
      <table className='table'>
        <thead className='thead-dark'>
          <tr>
            <th scope="col">name</th>
            <th scope="col">last name</th>
            <th scope="col">age</th>
            <th scope="col">phone number</th>
            <th scope="col">adresse</th>
            <th scope='col'>formations</th>
            <th scope='col' colspan="3">action</th>
          </tr>
        </thead>
        <tbody>
          {employesList}
        </tbody>
      </table>
    </div>
  );
}

// Replace someAsyncOperation with your actual asynchronous operation function
const someAsyncOperation = async (formation) => {
  // Example asynchronous operation
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(`Additional data for ${formation.name}`);
    }, 100);
  });
};
