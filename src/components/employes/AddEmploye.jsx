import { useEffect, useRef, useState } from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
export default function AddEmploye() {
    const navigate = useNavigate();
    const nameField = useRef()
    const lastnameField = useRef()
    const ageField = useRef()
    const phoneNumberField = useRef()
    const adresseField = useRef()
    const [formValues, setFormValues] = useState({})


    useEffect(() => {
        axios.get(`http://localhost:3003/formations`).then(response=>console.log(response.data))
    }, []);

    useEffect(() => {
        
        if (Object.keys(formValues).length > 0) {
            axios.post('http://localhost:3003/employees', formValues)
                .then(response => {
                    alert('the employe is added');
                    
                    setFormValues({});
                })
                .catch(error => {
                    alert('Error sending request:', error);
                });
                navigate('/employes')
        }
    }, [formValues]);

    const handleSubmit = (e) => {
        e.preventDefault();

        // Update the state using the callback form
        setFormValues({
            name: nameField.current.value,
            lastname: lastnameField.current.value,
            age: ageField.current.value,
            phone_number: phoneNumberField.current.value,
            address: adresseField.current.value,
        });
    };

    return <div className="container w-50 mt-5">
        <form onSubmit={handleSubmit}>
            <div className="mb-3 ">
                <label htmlFor="name" className="form-label">Name :</label>
                <input
                    type="text"
                    className="form-control"
                    name="name"
                    id="name"
                    placeholder="name"
                    ref={nameField}
                />
            </div>
            <div className="mb-3">
                <label htmlFor="lastname" className="form-label">Last name :</label>
                <input
                    type="text"
                    className="form-control"
                    name="lastname"
                    id="lastname"
                    placeholder="lastname"
                    ref={lastnameField}
                />
            </div>
            <div className="mb-3">
                <label htmlFor="age" className="form-label">Age :</label>
                <input
                    type="number"
                    className="form-control"
                    name="age"
                    id="age"
                    placeholder="age"
                    ref={ageField}
                />
            </div>
            <div className="mb-3">
                <label htmlFor="phone_number" className="form-label">Phone Number :</label>
                <input
                    type="number"
                    className="form-control"
                    name="phone_number"
                    id="phone_number"
                    placeholder="phone_number"
                    ref={phoneNumberField}
                />
            </div>
            <div className="mb-3">
                <label htmlFor="adresse" className="form-label">Adresse :</label>
                <input
                    type="text"
                    className="form-control"
                    name="adresse"
                    id="adresse"
                    placeholder="adresse"
                    ref={adresseField}
                />
            </div>
           

            <div className="mb-3">
                <input type='submit' value="Ajouter" className="btn btn-primary" onClick={handleSubmit}></input>
            </div>
        </form>

    </div>
}