import axios from 'axios'
import React, { useEffect, useRef, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'

function UpdateEmploye() {
  const navigate = useNavigate();
    const nameField = useRef()
    const lastnameField = useRef()
    const ageField = useRef()
    const phoneNumberField = useRef()
    const adresseField = useRef()
    const [formValues,setFormValues] = useState({})
    const  [employe,setEmploye] = useState({})
    let params = useParams()
    useEffect(() => {
        axios.get(`http://localhost:3003/employees/${params.id}`)
          .then(response => setEmploye(response.data)).then();
      }, [params.id]);
      useEffect(() => {
        // This effect will run whenever formValues changes
        if (Object.keys(formValues).length > 0) {
            axios.put(`http://localhost:3003/employees/${params.id}`, formValues)
            .then(response => {
              console.log('Request sent successfully');
              // Optionally, you can reset the form values after successful submission
              setFormValues({});
              
            })
            .catch(error => {
              console.error('Error sending request:', error);
            });
            navigate('/employes')
        }
      }, [formValues]);

      const handleSubmit = async (e) => {
        e.preventDefault();
        setFormValues({
          name: nameField.current.value,
          lastname: lastnameField.current.value,
          age: ageField.current.value,
          phone_number: phoneNumberField.current.value,
          address: adresseField.current.value,
        });
      /*
        try {
        console.log(formValues)
          const response = await axios.put(`http://localhost:3003/employees/${params.id}`, formValues);
          console.log(response.data);
        } catch (error) {
          console.error('Error updating employee:', error);
        }
        */
      };
  return (
    <div className="container w-50 mt-5">
        <form onSubmit={handleSubmit}>
    <div className="mb-3 ">
        <label htmlFor="name" className="form-label">Name :</label>
        <input
            type="text"
            className="form-control"
            name="name"
            id="name"
            placeholder="name"
            ref={nameField}
            defaultValue={employe.name}
        />
    </div>
    <div className="mb-3">
        <label htmlFor="lastname" className="form-label">Last name :</label>
        <input
            type="text"
            className="form-control"
            name="lastname"
            id="lastname"
            placeholder="lastname"
            ref={lastnameField}
            defaultValue={employe.lastname}
        />
    </div>
    <div className="mb-3">
        <label htmlFor="age" className="form-label">Age :</label>
        <input
            type="number"
            className="form-control"
            name="age"
            id="age"
            placeholder="age"
            ref={ageField}
            defaultValue={employe.age}
        />
        </div>
        <div className="mb-3">
        <label htmlFor="phone_number" className="form-label">Phone Number :</label>
        <input
            type="text"
            className="form-control"
            name="phone_number"
            id="phone_number"
            placeholder="phone_number"
            ref={phoneNumberField}
            defaultValue={employe.phone_number}
        />
    </div>
    <div className="mb-3">
        <label htmlFor="adresse" className="form-label">Adresse :</label>
        <input
            type="text"
            className="form-control"
            name="adresse"
            id="adresse"
            placeholder="adresse"
            ref={adresseField}
            defaultValue={employe.address}
        />
    </div>

    
    <div className="mb-3">
    <input type='submit' value="modifier" className="btn btn-primary" onClick={handleSubmit}></input>
    </div>
    </form>
    
    </div>
  )
}

export default UpdateEmploye