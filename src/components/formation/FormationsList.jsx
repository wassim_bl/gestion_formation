
import React, { useState } from 'react'
import {Link} from 'react-router-dom'
import {useEffect} from 'react'
import { useDispatch, useSelector } from 'react-redux'
import axios from 'axios'
export default function FormationsList() {
  const startDate = useSelector(state=>state.startDate)
  const endDate = useSelector(state=>state.endDate)
  const etat = useSelector(state=>state.etat)
  //const formaList = useSelector(state=>state.list)
  const [formaList,setFormaList] = useState([])
  const dispatch = useDispatch()
  const [warning,setWarning] = useState(null)
  const getFormations = async () => {
    try {
      const response = await axios.get("http://localhost:3003/formations");
      const formations = response.data;
      const updatedFormations = await Promise.all(formations.map(async (forma) => {
        if (forma.etat === 'annulee') {
          return forma;
        }
        if (new Date() >= new Date(forma.date_debut)) {
          forma.etat = "en cours";
          await axios.put(`http://localhost:3003/formations/${forma.id}`, forma);
        }
        if (new Date() > new Date(forma.date_debut) && new Date() > new Date(forma.date_debut).setMonth(new Date(forma.date_debut).getMonth() + parseInt(forma.duration))) {
          forma.etat = "termine";
          await axios.put(`http://localhost:3003/formations/${forma.id}`, forma);
        }
    
        return forma;
      }));
      setFormaList(updatedFormations);
    } catch (error) {
      console.error('Error fetching or updating formations:', error);
    
    }
  };

  useEffect(()=>{
    getFormations()
  },[])


   const displayFormations = ()=>{
    let formaTemp = formaList
  
    if(etat){
      formaTemp = formaList.filter(ff=>{return ff.etat == etat})
    }
    if (startDate && endDate) {
      if (new Date(endDate) < new Date(startDate)) {
        setWarning('End date should be after start date');
      }
      formaTemp = formaTemp.filter(ff => {
        const formaDate = new Date(ff.date_debut);
        return formaDate >= new Date(startDate) && formaDate <= new Date(endDate);
      });
    }
    return formaTemp.map((forma,key)=>{
      return <tr key={key}><td>{forma.name}</td><td>{forma.duration}</td><td>{forma.difficulty}</td><td>{forma.date_debut}</td><td>{forma.etat}</td><td><Link to={'/formation/update/' + forma.id} className='btn btn-primary'>Update</Link></td></tr>
    })
  }
  const handleReset = ()=>{
    dispatch({type:'Etat',payload:{etat:''}})
    dispatch({type:'StartDate',payload:{startDate:''}})
    dispatch({type:'EndDate',payload:{endDate:''}})
  }
  return (
    <>
    {warning && <div className="alert alert-warning">{warning}</div>}
      {formaList.length > 0 && (
        <div className="container">
          <div>
            <Link to="/formation/add" className="btn btn-primary mt-5">Ajouter une formation</Link>
          </div>
          <div className="mb-3 d-flex flex-wrap align-items-center mt-3">
            <div className="me-3">
              <label htmlFor="etat" className="form-label">Etat</label>
              <select
                name="etat"
                id="etat"
                className="form-select"
                value={etat}
                onChange={(e) => {
                  dispatch({type:'Etat',payload:{etat:e.currentTarget.value}})
                }}
              >
                <option value="">All</option>
                <option value="programme">Programme</option>
                <option value="en cours">En cours</option>
                <option value="termine">Termine</option>
                <option value="annulee">annulee</option>
              </select>
            </div>
            <div className="d-flex align-items-center me-3">
              <div className="me-3">
                <label htmlFor="date_first" className="form-label">Date debut</label>
                <input
                  type="date"
                  className="form-control"
                  name="date_first"
                  id="date_first"
                  value={startDate}
                  onChange={(e) => {
                    dispatch({type:'StartDate',payload:{startDate:e.currentTarget.value}})
                  }}
                />
              </div>
              <div>
                <label htmlFor="date_seconde" className="form-label">Date Fin</label>
                <input
                  type="date"
                  className="form-control"
                  name="date_seconde"
                  id="date_seconde"
                  placeholder=""
                  value={endDate}
                  onChange={(e) => {
                    dispatch({type:'EndDate',payload:{endDate:e.currentTarget.value}})
                  }}
                />
              </div>
            </div>
            <div className="me-3">
              <div className="invisible">Actions</div>
              <button className="btn btn-info mt-1 me-3" onClick={handleReset}>Reset</button>
            </div>
          </div>
          <table className='table mt-5'>
            <thead className='thead-dark'>
              <tr>
                <th scope="col">Name</th>
                <th scope="col">Duration</th>
                <th scope="col">Difficulty</th>
                <th scope="col">Date debut</th>
                <th scope="col">Etat</th>
                <th scope="col" colspan="2" >Actions</th>
              </tr>
            </thead>
            <tbody>
              {displayFormations()}
            </tbody>
          </table>
        </div>
      )}
    </>
  );
}

