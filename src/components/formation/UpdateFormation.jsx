import { useRef, useState, useEffect } from 'react';
import { useParams,useNavigate} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'
import axios from 'axios';

export default function UpdateFormation() {
    const dispatch = useDispatch()
    const navigate = useNavigate();
    const { id } = useParams();
    const nameField = useRef();
    const durationField = useRef();
    const difficultyField = useRef();
    const date_debutField = useRef();
    const etatField = useRef();
    const [forma, setForma] = useState(null);
    const getFormation = () => {
        console.log(id);
        axios.get(`http://localhost:3003/formations?id=${id}`).then(res => setForma(res.data[0]));
    };

    useEffect(() => {
        getFormation();
    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();
        if (!nameField.current.value ||
            !durationField.current.value ||
            !difficultyField.current.value ||
            !date_debutField.current.value ||
            !etatField.current.value) {
            console.error("All fields are required.");
            return;
        }
        const newForm = {
            id: id,
            name: nameField.current.value,
            duration: durationField.current.value + ' months',
            difficulty: difficultyField.current.value,
            date_debut: date_debutField.current.value,
            etat: etatField.current.value
        };
        console.log(newForm);
        axios.put(`http://localhost:3003/formations/${id}`,newForm).then(()=>{
            navigate('/')
        })
        
    };

    useEffect(() => {
        if (forma) {
            nameField.current.value = forma.name;
            durationField.current.value = parseFloat(forma.duration);
            difficultyField.current.value = forma.difficulty;
            date_debutField.current.value = forma.date_debut;
            etatField.current.value = forma.etat;
        }
    }, [forma]);

    return (
        <>
            {forma && (
                <div className="container w-50 mt-5">
                    <form onSubmit={handleSubmit}>
                        <div className="mb-3">
                            <label htmlFor="name" className="form-label">Name</label>
                            <input
                                type="text"
                                className="form-control"
                                name="name"
                                id="name"
                                placeholder=""
                                ref={nameField}
                            />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="duration" className="form-label">Duration in months</label>
                            <input
                                type="number"
                                className="form-control"
                                name="duration"
                                id="duration"
                                placeholder="Mois"
                                ref={durationField}
                            />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="difficulty" className="form-label">Difficulty</label>
                            <select name="difficulty" id="difficulty" className="form-select" ref={difficultyField}>
                                <option value="Beginner">Beginner</option>
                                <option value="Intermediate">Intermediate</option>
                                <option value="Advanced">Advanced</option>
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="date_debut" className="form-label">Date debut</label>
                            <input
                                type="date"
                                className="form-control"
                                name="date_debut"
                                id="date_debut"
                                placeholder=""
                                ref={date_debutField}
                            />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="etat" className="form-label">Etat</label>
                            <select name="etat" id="etat" className="form-select" ref={etatField}>
                                <option value="programme">Programme</option>
                                <option value="en cours">En cours</option>
                                <option value="termine">Termine</option>
                                <option value="annulee">annulée</option>
                            </select>
                        </div>
                        <div className="mb-3">
                            <input type='submit' value="Update" className="btn btn-primary" />
                        </div>
                    </form>
                </div>
            )}
        </>
    );
}