import { useRef,useState,useEffect } from 'react'
import axios from 'axios'
import {useNavigate} from 'react-router-dom';
export default function AddFormation(){
    const navigate = useNavigate();
    const nameField = useRef()
    const durationField = useRef()
    const difficultyField = useRef()
    const date_debutField = useRef()
    const etatField = useRef()
    //const [formValues,setFormValues] = useState({})
    const [formaList,setFormaList] = useState([])
    const [lastId,setLastId] = useState({})
    const getFormations = ()=>{
        axios.get("http://localhost:3003/formations").then(res=>setFormaList(res.data))
      }
      useEffect(()=>{
        getFormations()
        
      },[])
      useEffect(()=>{
        const id = formaList.at(-1)
        setLastId(id)
      },[formaList,lastId])
      

    const handleSubmit = (e)=>{
        e.preventDefault()
        if (!nameField.current.value ||
            !durationField.current.value ||
            !difficultyField.current.value ||
            !date_debutField.current.value ||
            !etatField.current.value) {
            console.error("All fields are required.");
            return;
        }
        const newForm = {
            id:lastId.id+1,
            name:nameField.current.value,
            duration:durationField.current.value + ' months',
            difficulty:difficultyField.current.value,
            date_debut:date_debutField.current.value,
            etat:etatField.current.value
        }
        axios.post('http://localhost:3003/formations',newForm).then(response=>{
            console.log(response.data)
            setFormaList([...formaList, response.data]);})
        navigate('/')
        
    }
    return <><div className="container w-50 mt-5">
    <form onSubmit={handleSubmit}>
<div className="mb-3 ">
    <label htmlFor="name" className="form-label">Name</label>
    <input
        type="text"
        className="form-control"
        name="name"
        id="name"
        placeholder="Name"
        ref={nameField}
    />
</div>
<div className="mb-3">
    <label htmlFor="duration" className="form-label">Duration</label>
    <input
        type="number"
        className="form-control"
        name="duration"
        id="duration"
        placeholder="Mois"
        ref={durationField}
    />
</div>

    <div className="mb-3">
    <label htmlFor="difficulty" className="form-label">Difficulty</label>
    <select name="difficulty" id="difficulty" className="form-select" ref={difficultyField}>
        <option value="Beginner">Beginner</option>
        <option value="Intermediate">Intermediate</option>
        <option value="Advanced">Advanced</option>
    </select>
</div>
    <div className="mb-3">
    <label htmlFor="date_debut" className="form-label">Date debut</label>
    <input
        type="date"
        className="form-control"
        name="date_debut"
        id="date_debut"
        ref={date_debutField}
    />
</div>
<div className="mb-3">
    <label htmlFor="etat" className="form-label">Etat</label>
    <select disabled name="etat" id="etat" className="form-select" ref={etatField}>
        <option value="programme">Programme</option>
    </select>
</div>


<div className="mb-3">
<input type='submit' value="Ajouter" className="btn btn-primary"></input>
</div>
</form>

</div>
    </>
}