import React from 'react'
import { Outlet,Link} from 'react-router-dom';
function Layout() {
  return (
    <div className='container'>
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
          <li className="nav-item active">
            <Link to ={"/"} className="nav-link active" href="#">formations</Link>
          </li>
          <li className="nav-item">
          <Link to ={"/employes"} className="nav-link" href="#">employes</Link>
          </li>
          <li className="nav-item">
          <Link to ={"/participations"}className="nav-link" href="#">participations</Link>
          </li>
        </ul>
      </div>
    </nav>
    <Outlet/>
    </div>
    
  );
}

export default Layout