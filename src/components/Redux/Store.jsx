import {createStore} from 'redux'
import { formationsReducer } from './Formations/FormationsReducer'
export const store = createStore(formationsReducer)