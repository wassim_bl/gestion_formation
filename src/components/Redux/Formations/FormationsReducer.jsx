export const formationsReducer = (state={list:[],startDate:'',endDate:'',etat:''},action)=>{
    switch(action.type){
        case 'List':
            return {
                ...state,list:action.payload.list
            }
        case 'StartDate':
                return {
                    ...state,startDate:action.payload.startDate
                }
        case 'EndDate':
                return {
                    ...state,endDate:action.payload.endDate
                }   
        case 'Etat':
                return {
                    ...state,etat:action.payload.etat
                }       
        default:
            return state
    }
}