
import React from 'react'
import { Link } from 'react-router-dom'
import { useEffect, useState } from 'react'
import axios from 'axios'
export default function ParticipationList() {

  const [participations, setParticipation] = useState([])
  const getParticipation = () => {
    axios.get("http://localhost:3003/participation").then(res => setParticipation(res.data))
  }
  useEffect(() => {
    getParticipation()
  }, [participations])
  const displayFormations = () => {
    return participations.map((forma, key) => {
      return <tr key={key}><td>{forma.employee_name}</td><td>{forma.formations.map(forma => <li>{forma.name} <hr /></li>)}</td><td><Link to={'/updateparticipation/' + forma.id} className='btn btn-primary'>Update</Link></td><td><a href="" className='btn btn-danger' onClick={(e) => { e.preventDefault(); delete_participation(forma.id); }} >delete</a></td></tr>
    })
  }

  const delete_participation = (id) => {
    try {
      axios.delete(`http://localhost:3003/participation/${id}`)
      setParticipation(participations.filter(employe => employe.id !== id))
    }
    catch (error) {
      console.error('Error deleting employee:', error);
    }
  }


  return <> {participations.length > 0 &&
    <div>
      <div className="container">
        <div>

          <Link to="/participation/add" className="btn btn-primary mt-5">Ajouter une participation</Link>
        </div>
        <table className='table mt-5'>
          <thead className='thead-dark'>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">formations</th>
              <th scope='col' colspan="3">action</th>
            </tr>
          </thead>
          <tbody>
            {displayFormations()}
          </tbody>
        </table>
      </div>
    </div>}</>
}

