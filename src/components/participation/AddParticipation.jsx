import { useEffect, useRef, useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

export default function AddParticipation() {
    const navigate = useNavigate();
    const formationsField = useRef();
    const employeField = useRef();
    const [formValues, setFormValues] = useState({});
    const [employees, setEmployees] = useState([]);
    const [formations, setFormation] = useState([]);

    useEffect(() => {
        axios.get('http://localhost:3003/employees')
            .then(response => {
                setEmployees(response.data);
            })
            .catch(error => {
                console.error('Erreur lors de la récupération des employés', error);
            });
    }, []);

    useEffect(() => {
        axios.get('http://localhost:3003/formations')
            .then(response => {
                setFormation(response.data);
            })
            .catch(error => {
                console.error('Erreur lors de la récupération des employés', error);
            });
    }, []);

    useEffect(() => {
        if (Object.keys(formValues).length > 0) {
            axios.post('http://localhost:3003/participation', formValues)
                .then(response => {
                    alert('the participation is added');
                    setFormValues({});
                })
                .catch(error => {
                    alert('Error sending request:', error);
                });
            navigate('/participations');
        }
    }, [formValues]);

    const handleSubmit = (e) => {
        e.preventDefault();
        const selectedOptions = Array.from(formationsField.current.selectedOptions).map(option => ({
            name: option.value
        }));
        const selectedEmployeeId = employeField.current ? employeField.current.value : '';
        const selectedEmployee = employees.find(employee => employee.id === parseInt(selectedEmployeeId, 10));
        setFormValues({
            employee_id: selectedEmployeeId ? parseInt(selectedEmployeeId, 10) : 0,
            employee_name: selectedEmployee ? selectedEmployee.name : '',
            formations: selectedOptions
        });
        
    };

    return (
        <div className="container w-50 mt-5">
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <label>Sélectionnez un employé :</label>
                    <select className="form-select" ref={employeField}>
                        <option value="">Sélectionnez un employé</option>
                        {employees.map((employee) => (
                            <option key={employee.id} value={employee.id}>
                                {employee.name} {employee.lastname }
                            </option>
                        ))}
                    </select>
                </div>
                <br />
                <div className="mb-3">
                    <label>Sélectionnez une formation :</label>
                    <select className="form-select" ref={formationsField} multiple>
                        <option value="">Sélectionnez une formation</option>
                        {formations.map((formation) => (
                            <option key={formation.id} value={formation.name}>
                                {formation.name}
                            </option>
                        ))}
                    </select>
                </div>

              

                <div className="mb-3">
                    <input type="submit" value="Ajouter" className="btn btn-primary" />
                </div>
            </form>
        </div>
    );
}