import axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

function UpdateParticipation() {
    const navigate = useNavigate();
    const formationsField = useRef();
    const employeField = useRef();
    const [formValues, setFormValues] = useState({});
    const [employees, setEmployees] = useState([]);
    const [formations, setFormations] = useState([]);
    const params = useParams();

    useEffect(() => {
        axios.get('http://localhost:3003/formations')
            .then(response => {
                setFormations(response.data);
            })
            .catch(error => {
                console.error('Erreur lors de la récupération des formations', error);
            });
    }, []);

    useEffect(() => {
        axios.get(`http://localhost:3003/employees/${params.id}`)
            .then(response => setEmployees([response.data]));
    }, [params.id]);


    useEffect(() => {
        if (Object.keys(formValues).length > 0) {
            axios.put(`http://localhost:3003/participation/${params.id}`, formValues)
                .then(response => {
                    alert('Request sent successfully');
                    setFormValues({});
                })
                .catch(error => {
                    alert('Error sending request:', error);
                });
                navigate('/participations');
        }
    }, [formValues, params.id]);

    const handleSubmit = (e) => {
        e.preventDefault();
        const selectedOptions = Array.from(formationsField.current.selectedOptions).map(option => ({
            name: option.value
        }));
        const selectedEmployeeId = employeField.current ? employeField.current.value : '';
        const selectedEmployee = employees.find(employee => employee.id === parseInt(selectedEmployeeId, 10));
        setFormValues({
            employee_id: selectedEmployeeId ? parseInt(selectedEmployeeId, 10) : 0,
            employee_name: selectedEmployee ? selectedEmployee.name : '',
            formations: selectedOptions
        });
        
    };

    return (
        <div className="container w-50 mt-5">
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <label>Sélectionnez un employé :</label>
                    <select className="form-select" ref={employeField} required>
                        {employees.map((employee) => (
                            <option key={employee.id} value={employee.id}>
                                {employee.name}
                            </option>
                        ))}
                    </select>
                </div>
                <br />
                <div className="mb-3">
                    <label>Sélectionnez une formation :</label>
                    <select className="form-select" ref={formationsField} required multiple>
                        <option value="">Sélectionnez une formation</option>
                        {formations.map((formation) => (
                            <option key={formation.id} value={formation.name}>
                                {formation.name}
                            </option>
                        ))}
                    </select>
                </div>

               

                <div className="mb-3">
                    <input type="submit" value="Modifier" className="btn btn-primary" />
                </div>
            </form>
        </div>
    );
}

export default UpdateParticipation;
